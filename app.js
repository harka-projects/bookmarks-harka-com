const flattenBookmarks = bookmarks => {
  const flattenedBookmarks = []
  const flatten = b => {
    if (b.type === 'url') {
      const { name, url, meta_info } = b
      const flattenedBookmark = { name, url }
      if (meta_info) {
        const metaInfo = {}
        if (meta_info.Nickname) metaInfo.Nickname = meta_info.Nickname
        if (meta_info.Description) metaInfo.Description = meta_info.Description
        if (Object.keys(metaInfo).length) flattenedBookmark.meta_info = metaInfo
      }
      flattenedBookmarks.push(flattenedBookmark)
    }
    if (b.children && b.children.length) {
      for (const child of b.children) flatten(child)
    }
  }
  for (const root of Object.values(bookmarks.roots)) flatten(root)
  return flattenedBookmarks
}
let b = '/Users/d.harka/Library/Application Support/Vivaldi/Default/Bookmarks'
b = await Bun.file(b).text()
Bun.write('bookmarks-backup.json', b)
b = `const b = ${JSON.stringify(flattenBookmarks(JSON.parse(b)))};
t.addEventListener('keyup', () => {
  let l = '', i = b.length, txt = t.value.toUpperCase();
  if (txt.length) {
    while (i--) {
      if (
        b[i].url.toUpperCase().includes(txt) ||
        b[i].name.toUpperCase().includes(txt) ||
        b[i].meta_info?.Nickname?.toUpperCase().includes(txt) ||
        b[i].meta_info?.Description?.toUpperCase().includes(txt)
      ) {
        l += \`<br><a href="\${b[i].url}" target="_blank"
        tabindex="0" rel="noreferrer noopener nofollow">\${b[i].name}</a><br>\`
      }
    }
  }
  u.innerHTML = l;
});`
await Bun.write('js.js', b)
